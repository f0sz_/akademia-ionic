angular.module('starter.controllers', [])

  .controller('DashCtrl', function ($scope) { })
  .controller('LoginCtrl', function ($scope, $stateParams, FormsService) {
    $scope.model = {
      email: '',
      password: '',
      res: ''
    };

    $scope.options = {
      loop: false,
      speed: 500,
      resistanceRatio: 0
    }
    $scope.slider;
    $scope.flashActive = false;
    $scope.reversedCamera = false;

    $scope.scan = function () {
      QRScanner.scan(function (err, data) {
        try {
          var data = JSON.parse(data);
          if (data.email && data.verification_code__c) {
            navigator.vibrate(100);
            FormsService.login(data.email, data.verification_code__c)
              .then(function (res) {
                $scope.slider.slideTo(0); //slider
                $scope.model.res = res;
              })
              .catch(function (err) {
                console.log(err)
              })
          }
        }
        catch (err) {
          navigator.vibrate([50, 50, 50])
          console.log(err)
        }

      })
    }

    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
      $scope.slider = data.slider;
    });

    $scope.$on('$ionicSlides.slideChangeEnd', function (event, data) {
      if ($scope.slider.activeIndex === 1) {
        QRScanner.prepare(function (err, status) {
          if (err) console.error(err);
          if (status.authorized) {
            QRScanner.show();
          } else if (status.denied) {
            QRScanner.openSettings();
          } else {
            // no permissions
          }
        });
      } else {
        QRScanner.destroy();
        $scope.flashActive = false;
        $scope.reversedCamera = false;
      }
    })

    $scope.swipeToScanner = function () {
      $scope.slider.slideTo(1)
    }

    $scope.toggleFlash = function () {
      console.log($scope.flashActive)
      QRScanner.getStatus(function (status) {
        if (status.canEnableLight) {
          $scope.flashActive
            ? QRScanner.disableLight(function (err, status) {
              $scope.flashActive = !$scope.flashActive;
            })
            : QRScanner.enableLight(function (err, status) {
              $scope.flashActive = !$scope.flashActive;
            })
        }
      })
    }

    $scope.switchCamera = function () {
      QRScanner.getStatus(function (status) {
        if (status.currentCamera === 0) {
          QRScanner.useFrontCamera();
          $scope.reversedCamera = !$scope.reversedCamera;
        }

        if (status.currentCamera === 1) {
          QRScanner.useBackCamera();
          $scope.reversedCamera = !$scope.reversedCamera;
        }
      })
    }

    $scope.login = function () {
      FormsService.login($scope.model.email, $scope.model.password)
        .then(function (res) {
          $scope.model.res = res;
        })
        .catch(function (err) {
          $scope.model.res = err;
        })

      $scope.model.email = '';
      $scope.model.password = '';
    }

  })

  .controller('ResetCtrl', function ($scope, $stateParams, FormsService) {
    $scope.model = {
      email: '',
      res: '',
      newPassword: ''
    }
    $scope.resetPassword = function () {
      FormsService.resetPassword($scope.model.email)
        .then(function (res) {
          $scope.model.res = res;
          $scope.model.newPassword = res.data.payload.verification_code__c;
        })
        .catch(function (err) {
          $scope.model.res = err;
        })
      $scope.model.email = '';
    }
  })
