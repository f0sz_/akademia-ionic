angular.module('starter.directives', [])

  .directive('serverResponse', () => {
    return {
      restrict: 'E',
      templateUrl: 'templates/server-response.html',
      scope: {
        model: '='
      },
      controller: ($scope) => {

      }
    }
  })
